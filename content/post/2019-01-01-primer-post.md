---
title: Primer post :D
subtitle: Cristian Ros
date: 2019-01-01
tags: ["post", "personal"]
bigimg: [{src: "/img/triangle.jpg", desc: "Triangle"}, {src: "/img/sphere.jpg", desc: "Sphere"}, {src: "/img/hexagon.jpg", desc: "Hexagon"}]
---

Este es mi blog personal para mis proyectos, experiencias, tutoriales, reviews, críticas constructivas e información sobre los proyectos del software libre y codigo abierto.

<!--more-->

Espero le sirva a mas de uno mis experiencias, posiblemente no les guste a algunos y a otros si, pero, lo mas importante para mi es que este blog va dirijido al que le interese y al que quiera aprender algo nuvo o le interesa saber más sobre este mundillo.
Posiblemente haré seccines para programadores, diseñadores, músicos, productores en el ambito multimedia, podcasters, youtuber y alguno que otra área de la cual me surge hablar.
En cuanto a los videojuegos solamente reviews, tutoriales para resolver alguno que otro problema o criticas constructivas; y es posible que desarrolle algun juego indie en un futuro así que espero apoyen comentadon compartindo sus ideas o alguna critica constructiva hacia el tema correspondiente.
Para mas información entra al apartado de AYUDA ahí encontraran mas información. Comenta para saber tu opinion, critica, ayuda (tanto si necesistas como a mi persona, en caso de que necesitas vere si esta en mis posibilidades para ayudarte).
